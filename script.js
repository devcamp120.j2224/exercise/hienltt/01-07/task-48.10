$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gNameCol = ['id', 'studentId', 'subjectId', 'grade', 'examDate', 'action'];
    const gSTT_COL = 0;
    const gSTUDENT_ID_COL = 1;
    const gSUBJECT_ID_COL = 2;
    const gGRADE_COL = 3;
    const gEXAM_DATE_COL = 4;
    const gACTION_COL = 5;
    var gStt = 0;
    var gSubjects = [];
    var gStudents = [];
    var gGrades = [];
    var gIdOfGrade = "";
    // định nghĩa table
    var gStudentTable = $("#student-table").DataTable({
        "columns": [
            { "data": gNameCol[gSTT_COL] },
            { "data": gNameCol[gSTUDENT_ID_COL] },
            { "data": gNameCol[gSUBJECT_ID_COL] },
            { "data": gNameCol[gGRADE_COL] },
            { "data": gNameCol[gEXAM_DATE_COL] },
            { "data": gNameCol[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<button class='btn btn-primary btn-edit'>Edit</button>
                                <button class='btn btn-danger btn-delete'>Delete</button>`
            },
            {
                targets: gSTT_COL,
                render: function () {
                  gStt++;
                  return gStt;
                }
            },
            {
                targets: gSTUDENT_ID_COL,
                render: function(data){
                    var vStudentName = renderToStudentNameByStudentId(data);
                    return vStudentName;
                }
            },
            {
                targets: gSUBJECT_ID_COL,
                render: function(data){
                  var vSubjectName = renderToSubjectNameBySubjectd(data);
                  return vSubjectName;
                },
            }
        ],
        paging: false,
        searching: false,
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // load dữ liệu lên select stundent
    loadDataToStudentSelect();
    // load dữ liệu lên select subject
    loadDataToSubjectSelect();
    // Gán event handler cho sự kiện click icon Filter
    $('#btn-filter').on('click', function(){
        onIconFilterClick();
    });
    // Gán event handler cho nút Delete
    $('#student-table').on('click', '.btn-delete', function(){
        onBtnDeleteClick(this);
    })
    // Gán event handler cho nutus confirm delete trên modal
    $('#btn-confirm-delete').on('click', function(){
        onBtnModalConfirmDeleteClick();
    })
    // Gán event handler cho nút Edit
    $("#student-table").on('click', '.btn-edit', function(){
        onBtnEditClick(this);
    });
    // Gán event handler cho nút Update trên modal
    $('#btn-modal-edit').on('click', function(){
        onBtnModalUpdateClick();
    })
    // Gán event handler cho nút Thêm
    $('#btn-add').on('click', function(){
        onBtnAddClick();
    });
    // Gán event handler cho nút Thêm trong modal
    $('#btn-add-grade').on('click', function(){
        onBtnModalAddClick();
    });
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm xử lý load trang
    function onPageLoading(){
        "use strict";
        // Gọi API để lấy danh sách điểm - get all (grades)
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//grades",
            type: "GET",
            dataType: 'json',
            sync: false,
            success: function(res){
                console.log(res);
                gStt = 0;
                gGrades = res;
                var vArrResult = [];
                for (var bI = 0; bI < res.length; bI ++){
                    if(res[bI].studentId !== null){
                        vArrResult.push(res[bI]);
                    }
                }
                loadDataToTable(vArrResult);
            },
            error: function(error){
                console.alert(error.responseText);
            }
        });       
    }
    // Hàm load dữ liệu vào ô Select Student
    function loadDataToStudentSelect() {
        "use strict";
        //Goi API lấy danh sách sinh viên
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//students",
            type: 'GET',
            dataType: 'json',
            success: function(res){
                gStudents = res;
                console.log(gStudents);

                var vStudentSelect = $("#select-student");
                var vStudentSelectModalEdit = $('#select-edit-modal-student');
                var vStudentSelectModalAdd = $('#select-add-modal-student');
                $("<option/>", {
                    text: "--- Chọn ---",
                    value: 0
                }).appendTo(vStudentSelect);
                for (let bI = 0; bI < res.length; bI++) {
                    var bOption = $("<option/>", {
                        text: res[bI].firstname + " " + res[bI].lastname,
                        value: res[bI].id
                    }).appendTo(vStudentSelect);

                    var bOption = $("<option/>", {
                        text: res[bI].firstname + " " + res[bI].lastname,
                        value: res[bI].id
                    }).appendTo(vStudentSelectModalEdit);

                    var bOption = $("<option/>", {
                        text: res[bI].firstname + " " + res[bI].lastname,
                        value: res[bI].id
                    }).appendTo(vStudentSelectModalAdd); 
                }                
            },
            error: function(error){
                alert(error.responeText);
            }
        });
    }
    // Hàm load dữ liệu vào ô Select Subject
    function loadDataToSubjectSelect() {
    "use strict";
    //Goi API lấy danh sách môn học
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//subjects",
            type: 'GET',
            dataType: 'json',
            success: function(res){
                gSubjects = res;
                console.log(gSubjects);
                var vSubjectSelect = $("#select-subject");
                var vSubjectSelectModalEdit = $('#select-edit-modal-subject');
                var vSubjectSelectModalAdd = $('#select-add-modal-subject');
                $("<option/>", {
                    text: "--- Chọn ---",
                    value: 0
                }).appendTo(vSubjectSelect);
                for (let bI = 0; bI < res.length; bI++) {
                    var bOption = $("<option/>", {
                        text: res[bI].subjectName,
                        value: res[bI].id
                    }).appendTo(vSubjectSelect);

                    var bOption = $("<option/>", {
                        text: res[bI].subjectName,
                        value: res[bI].id
                    }).appendTo(vSubjectSelectModalEdit);

                    var bOption = $("<option/>", {
                        text: res[bI].subjectName,
                        value: res[bI].id
                    }).appendTo(vSubjectSelectModalAdd);
                }
            },
            error: function(error){
                alert(error.responeText);
            }
        });    
    }
    // Hàm xử lý khi nhấn icon Filter
    function onIconFilterClick(){
        "use strict";
        //B0: Khai báo đối tượng lưu dữ liệu
        var vFilterData = {
            studentID: -1,
            subjectID: -1
        };
        //B1: Thu thập dữ liệu
        getDataNeedToFilter(vFilterData);
        console.log(vFilterData);
        //B2: validate ( ko cần)
        //B3: xử lý filter
        var vResultFilter = filterData(vFilterData);
        console.log(vResultFilter);
        //B4: hiển thị dữ liệu lên table
        if (vResultFilter[0].length > 0){
            displayResultFilterToTable(vResultFilter);
        }
        else {
            $('#student-table tbody').html("");
            alert("Không tìm thấy dữ liệu hợp lệ!");
        }  
    }
    // Hàm xử lý khi nhấn vào nút Delete trong table
    function onBtnDeleteClick(paramBtn){
        "use strict";
        //lấy ra data user tương ứng nút được click
        var vDataCurrentRow = getDataByButton(paramBtn);
        gIdOfGrade = vDataCurrentRow.id;
        console.log("Id tương ứng: " + vDataCurrentRow.id);
        $('#delete-confirm-modal').modal();        
    }
    // Hàm xử lý khi nhấn vào nút Delete trong table
    function onBtnEditClick(paramBtn){
        "use strict";
        //lấy ra data user tương ứng nút được click
        var vDataCurrentRow = getDataByButton(paramBtn);
        gIdOfGrade = vDataCurrentRow.id;
        console.log("Id tương ứng: " + vDataCurrentRow.id);
        showDataGradeToModal(gIdOfGrade);
        $('#edit-grade-modal').modal();        
    }
    // Hàm xử lý sự kiện confirm delete trên modal
    function onBtnModalConfirmDeleteClick(){
        "use strict";
        //B1: thu thập dữ liệu
        //B2: validate
        //B3: Gọi APi delete grade theo id
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//grades/" + gIdOfGrade,
            type: "DELETE",
            dataType: 'json',
            success: function(res){
                console.log("Đã xoá grade có id là: " + gIdOfGrade);
                alert("Xoá Grade có ID " + gIdOfGrade + " thành công!");
                onPageLoading(); //gọi hàm tải trang để load lại table
                $('#delete-confirm-modal').modal('hide');
            },
            error: function(error){
                alert(error.responeText);
            }
        });
    }
    // Hàm xử lý sự kiện update trên modal
    function onBtnModalUpdateClick(){
        "use strict";
        // Khai báo đối tượng chứa data
        var vInfoGradeObj = {
            studentId: "",
            subjectId: "",
            grade: "",
            examDate: ""
        };
        //B1: thu thập dữ liệu để update
        getDataToUpdate(vInfoGradeObj);
        //B2: validate dữ liệu 
        var vCheck = validateDataInputModalUpdate(vInfoGradeObj);
        console.log(vCheck);
        if(vCheck){
            $.ajax({
                url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//grades/" + gIdOfGrade,
                type: "PUT",
                contentType: 'application/json;charset=utf-8',
                data: (JSON.stringify(vInfoGradeObj)),
                success: function(res){
                    console.log(res);
                    alert("Cập nhật thành công!");                    
                    onPageLoading();
                    $('#edit-grade-modal').modal('hide');
                },
                error: function(res){
                    alert(error.responeText);
                }
            })        
        }
    }
    // hàm xử lý khi nhấn nút Thêm
    function onBtnAddClick(){
        "use strict";
        $('#add-grade-modal').modal();
    }
    // hàm xử lý khi nút thêm trong modal được nhấn
    function onBtnModalAddClick(){
        "use strict";;
        // Khai báo đối tượng chứa data
        var vGradeObj = {
            studentId: "",
            subjectId: "",
            grade: "",
            examDate: ""
        };
        //B1: thu thập dữ liệu để add
        getDataToAdd(vGradeObj);
        //B2: validate dữ liệu 
        var vCheck = validateDataInputModalAdd(vGradeObj);
        console.log(vCheck);
        if(vCheck){
            $.ajax({
                url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1//grades",
                type: "POST",
                contentType: 'application/json;charset=utf-8',
                data: (JSON.stringify(vGradeObj)),
                success: function(res){
                    console.log(res);
                    alert("Thêm dữ liệu thành công!");                    
                    onPageLoading();
                    $('#add-grade-modal').modal('hide');
                },
                error: function(res){
                    alert(error.responeText);
                }
            })        
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load data to table
    function loadDataToTable(paramResponseObject) {
        //Xóa toàn bộ dữ liệu đang có của bảng
        gStudentTable.clear();
        //Cập nhật data cho bảng 
        gStudentTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        gStudentTable.draw();
    }
    // Hàm render từ student ID sang student name
    function renderToStudentNameByStudentId(paramData) {
        "use strict";
        for (let bI = 0; bI < gStudents.length; bI++) {
            if (paramData == gStudents[bI].id) {
                var vFullNameStudent = gStudents[bI].firstname + " " + gStudents[bI].lastname;
                return vFullNameStudent;
            }  
        }
    }
    // Hàm render từ subject ID sang subject name
    function renderToSubjectNameBySubjectd(paramData) {
        "use strict";
        for (let bI = 0; bI < gSubjects.length; bI++) {
            if (paramData == gSubjects[bI].id) {
                var vSubjectName = gSubjects[bI].subjectName;
                return vSubjectName;
            }
        }
    }
    // Hàm thu thập dữ liệu để lọc
    function getDataNeedToFilter(paramData){
        "use strict";
        paramData.studentID = parseInt( $('#select-student option:selected').val(), 10); //chuyển về kiểu number
        paramData.subjectID = parseInt( $('#select-subject option:selected').val(), 10);
    }
    // Hàm xử lý lọc dữ liệu
    function filterData(paramObj){
        "use strict";
        const bArrResultObj = [];
        //Trường hợp cả 2 select student và subject đều được chọn
        if (paramObj.studentID != 0 && paramObj.subjectID != 0){
        var vFilter = gGrades.filter(function(item){
            return (item.studentId == paramObj.studentID && item.subjectId == paramObj.subjectID );
        });
        bArrResultObj.push(vFilter);
        }
        //Trường hợp chỉ 1 trong 2 select được chọn
        else if (paramObj.studentID != 0 || paramObj.subjectID != 0){
        var vFilter = gGrades.filter(function(item){
            return (item.studentId == paramObj.studentID || item.subjectId == paramObj.subjectID);
        });
        bArrResultObj.push(vFilter);
        }
        else if (paramObj.studentID == 0 && paramObj.subjectID == 0){
        var vFilter = gGrades.filter(function(item){
            return ( paramObj.studentID == 0);
        });
        bArrResultObj.push(vFilter);
        }
        return bArrResultObj;
    }
    // Hàm đổ lại dữ liệu vào datatable theo kết quả lọc
    function displayResultFilterToTable(paramObj){
        "use strict"
        gStt = 0;
        var vTable = $('#student-table').DataTable();
        vTable.clear();
        vTable.rows.add(paramObj[0]);
        vTable.draw();
    }
    // Hàm lấy ra dữ liệu của hàng chứa icon được click
    function getDataByButton(paramBtn){
        "use strict";
        var vCurrentRow = $(paramBtn).closest('tr'); // xác định row chứa icon được click
        var vDataTable = $('#student-table').DataTable(); //truy xuất đến bảng
        var vCurrentRowData = vDataTable.row(vCurrentRow).data();
        return vCurrentRowData;
    }
    // Hàm show course obj lên modal
    function showDataGradeToModal(paramIdGrade){
        "use strict";
        var vGradeIndex = getGradeIndexFromId(paramIdGrade);
        $('#select-edit-modal-student').val(gGrades[vGradeIndex].studentId);
        $('#select-edit-modal-subject').val(gGrades[vGradeIndex].subjectId);
        $('#inp-edit-modal-grade').val(gGrades[vGradeIndex].grade);
        $('#inp-edit-modal-testday').val(gGrades[vGradeIndex].examDate);
    }
    // Hàm lấy course index from course id
    function getGradeIndexFromId(paramIdGrade){
        "use strict";
        var vGradeIndex = -1;
        var vGradeFound = false;
        var vLoopIndex = 0;
        while( !vGradeFound && vLoopIndex < gGrades.length){
            if(gGrades[vLoopIndex].id === paramIdGrade){
                vGradeIndex = vLoopIndex;
                vGradeFound = true;
            }
            else {
                vLoopIndex ++;
            }
        }
        return vGradeIndex;
    }
    // Hàm thu thập dữ liệu để update
    function getDataToUpdate(paramInput){
        "use strict";
        paramInput.studentId = $('#select-edit-modal-student').val();
        paramInput.subjectId = $('#select-edit-modal-subject').val();
        paramInput.grade = $('#inp-edit-modal-grade').val().trim();
        paramInput.examDate = $('#inp-edit-modal-testday').val().trim();
    }
    // Hàm validate data để update
    function validateDataInputModalUpdate(paramInput){
        "use strict";
        var vPattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/; // validate kiểu dd/mm/yyyy
        if(paramInput.grade == ""){
            alert("Hãy nhập Điểm!")
            return false;
        }
        else if(isNaN(paramInput.grade) || Number(paramInput.grade) < 0 || Number(paramInput.grade) > 10){
            alert("Điểm phải là số từ 0 đến 10!")
            return false;
        }
        else if(paramInput.examDate == ""){
            alert("Hãy nhập Ngày thi!")
            return false;
        }
        else if(!vPattern.test(paramInput.examDate)){
            alert("Ngày thi phải có định dạng dd/mm/yyyy!")
            return false;
        }
        return true;
    }
    // Hàm thu thập dữ liệu để add
    function getDataToAdd(paramInput){
        "use strict";
        paramInput.studentId = $('#select-add-modal-student').val();
        paramInput.subjectId = $('#select-add-modal-subject').val();
        paramInput.grade = $('#inp-add-modal-grade').val().trim();
        paramInput.examDate = $('#inp-add-modal-testday').val().trim();
    }
    // Hàm validate data để add
    function validateDataInputModalAdd(paramInput){
        "use strict";
        var vPattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/; // validate kiểu dd/mm/yyyy
        if(paramInput.studentId == "none"){
            alert("Hãy chọn Sinh viên")
            return false;
        }
        else if(paramInput.subjectId == "none"){
            alert("Hãy chọn Môn học!")
            return false;
        }
        else if(paramInput.grade == ""){
            alert("Hãy nhập Điểm!")
            return false;
        }
        else if(isNaN(paramInput.grade) || Number(paramInput.grade) < 0 || Number(paramInput.grade) > 10){
            alert("Điểm phải là số từ 0 đến 10!")
            return false;
        }
        else if(paramInput.examDate == ""){
            alert("Hãy nhập Ngày thi!")
            return false;
        }
        else if(!vPattern.test(paramInput.examDate)){
            alert("Ngày thi phải có định dạng dd/mm/yyyy!")
            return false;
        }
        return true;
    }
});
  
